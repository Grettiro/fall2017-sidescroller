﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryHandler : MonoBehaviour {

    private StoryPassageCache storyCache;
    public List<TextAsset> storyIndex;
	private StoryDialogMenu m_StoryDialogMenu;
    private PlayerMovement playerMovement;
    private PauseScript pauseScript;
    private bool storyEnabled = false;
    public bool allowSkip = false;
    public int storyIndexFrom;
    public int storyIndexTo;
    private int storyCounter = 0;
    // Use this for initialization
    void Start () {
		m_StoryDialogMenu = FindObjectOfType<StoryDialogMenu> ();
        storyCache = new StoryPassageCache();
        playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        pauseScript = GameObject.Find("StoryCanvas").GetComponent<PauseScript>();
        storyIndex = storyCache.StoryCacheReturn(storyIndexFrom, storyIndexTo);
	}

    private void Update()
    {
        if(storyEnabled)
        {
            if(Input.anyKeyDown)
            {
                if (allowSkip)
                {
                    m_StoryDialogMenu.textSpeed = 0.025f;
                    NextStoryText();
                    allowSkip = false;
                }
                else
                {
                    m_StoryDialogMenu.textSpeed = 0;
                }
            }
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                DisableStoryText();
            }
        }
    }

    public void ActivateStoryText()
    {
        storyEnabled = true;
        m_StoryDialogMenu.DisplayDialog(storyIndex[storyCounter].text);
        playerMovement.animatorState.SetFloat("Speed", 0f);
        playerMovement.enabled = false;
        pauseScript.enabled = false;

    }
    void NextStoryText()
    {
        storyCounter++;
        if (storyCounter == storyIndex.Count)
        {
            DisableStoryText();
            storyEnabled = false;
        }
        else
        {
            m_StoryDialogMenu.DisplayDialog(storyIndex[storyCounter].text);
        }
    }
    void DisableStoryText()
    {
        playerMovement.enabled = true;
        pauseScript.enabled = true;
        storyEnabled = false;
		m_StoryDialogMenu.HideDialog ();
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ActivateStoryText();
        m_StoryDialogMenu.storyHandler = this;

    }
}
