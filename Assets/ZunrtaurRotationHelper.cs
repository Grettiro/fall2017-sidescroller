﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZunrtaurRotationHelper : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Rotate(int direction)
    {
        if(direction == 1)
        {
            StartCoroutine(Lerp(Quaternion.Euler(0, -90, 0)));
            //transform.localRotation = Quaternion.Euler(0, -90, 0);
        }
        else if(direction == 2)
        {
            //transform.localRotation = Quaternion.Euler(0, 90, 0);
            StartCoroutine(Lerp(Quaternion.Euler(0, 90, 0)));
        }
        else
        {
            //transform.localRotation = Quaternion.Euler(0, 0, 0);
            StartCoroutine(Lerp(Quaternion.Euler(0, 0, 0)));
        }
    }

    IEnumerator Lerp(Quaternion point)
    {
        float timeElapsed = 0;
        float time = 0.25f;
        Quaternion startingRotation = transform.localRotation;
        while (timeElapsed < time)
        {
            transform.localRotation = Quaternion.Lerp(startingRotation, point, (timeElapsed / time));
            timeElapsed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
}
