﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScript : MonoBehaviour {

	public GameObject PauseFrame;

    private bool paused = false;
    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!paused)
            {
                Pause();
            }
            else
            {
                Resume();
            }

        }
    }

    void Pause()
    {
        paused = !paused;
        Time.timeScale = 0;
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>().enabled = false;
		PauseFrame.SetActive (true);
    }

    public void Resume()
    {
        paused = !paused;
        Time.timeScale = 1;
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>().enabled = true;
		PauseFrame.SetActive (false);
    }

    public void Restart()
    {
		Debug.Log ("Restart");
		Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        
    }

    public void MainMenu()
    {
		Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
