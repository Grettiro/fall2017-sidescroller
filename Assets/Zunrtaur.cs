﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zunrtaur : Enemy {

    private List<Transform> waypoints = new List<Transform>();
    private Animator animator;
    private bool moving = false;
    private float moveDelay = 0;
    private float animLength;
    private float directionTimer;
    private int prevWaypoint = 1;
    public AudioSource walkSource;

    public Transform zurntaurTransform;

    // Use this for initialization
    protected override void Start()
    {
        StartCoroutine(PlayIdleSound());
        base.Start();
        facingLeft = false;
        animator = GetComponentInChildren<Animator>();
        animLength = animator.GetCurrentAnimatorStateInfo(0).length;
        fieldOfView = 180f;
        viewDistance = 12f;
        Transform[] tempWaypoints = transform.parent.GetComponentsInChildren<Transform>();
        foreach (Transform tWaypoint in tempWaypoints)
        {
            if (tWaypoint.name.Contains("Waypoint"))
            {
                waypoints.Add(tWaypoint);
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (!mindControlled)
        {
            if (currentStatus == enemyStatus.Idle)
            {
                if (Detect() && !attacking)
                {
                    SetStatus((int)enemyStatus.Attacking);
                }
                else
                {
                    if (!moving)
                    {
                        if (moveDelay >= animLength)
                        {
                            moving = true;
                            MoveRandom();
                            moveDelay = 0;
                        }
                        else
                        {
                            if (directionTimer >= animLength / 2)
                            {
                                eyes.transform.localRotation = Quaternion.Euler(0, 90, 0);
                            }
                            if (directionTimer >= animLength)
                            {
                                eyes.transform.localRotation = Quaternion.Euler(0, -90, 0);
                                directionTimer = 0;
                            }

                            moveDelay += Time.deltaTime;
                            directionTimer += Time.deltaTime;
                        }
                    }

                }
            }
            if (currentStatus == enemyStatus.Attacking)
            {
                if (!attacking)
                {
                    Attack();
                    attacking = true;
                    Debug.Log("Attacking");
                }
            }
            if (currentStatus == enemyStatus.Vulnerable)
            {
                if (vulnerableTimer >= 3f)
                {
                    currentStatus = enemyStatus.Idle;
                    animatorState.Play("Walk");
                    moving = true;
                    Vector3 furthestWaypoint = transform.position;
                    float waypointDistance = Vector3.Distance(transform.position, transform.position);
                    for (int i = 0; i < waypoints.Count; i++)
                    {
                        if ((Vector3.Distance(waypoints[i].position, transform.position) > waypointDistance))
                        {
                            furthestWaypoint = waypoints[i].position;
                            prevWaypoint = i;
                        }
                    }
                    StartCoroutine(Move(furthestWaypoint, 4f));
                    vulnerableTimer = 0;
                }
                else
                {
                    vulnerableTimer += Time.deltaTime;
                }
            }
        }
        else
        {
            float direction = Input.GetAxis("Horizontal");
            HandleMovement(direction);
        }
    }
    
    protected override void Attack()
    {
        if(moveSource.isPlaying == true)
        {
            StartCoroutine(AudioFadeOut(moveSource, 0.1f));
        }
        if (walkSource.isPlaying == true)
        {
            StartCoroutine(AudioFadeOut(walkSource, 0.1f));
        }
        attackSource.PlayOneShot(a_Attack);
        animatorState.Play("Attack");
        if (transform.position.x - player.transform.position.x < 0)
        {
            rotation.Rotate(1);
        }
        else
        {
            rotation.Rotate(2);
        }
        StartCoroutine(AttackDelay(0.3f));
    }

    IEnumerator AttackDelay(float time)
    {
        Vector3 playerPos = player.transform.position;
        yield return new WaitForSeconds(time);
        //Initialize float variables
        float gravity = Physics.gravity.magnitude * 2.5f;
        float angle = 10 * Mathf.Deg2Rad;
        //Initialize target position and origin position
        Vector3 planarTarget = new Vector3(playerPos.x, 0, playerPos.z);
        Vector3 planarPosition = new Vector3(transform.position.x, 0, transform.position.z);
        //Calculate distance and difference between y pos
        float distance = Vector3.Distance(planarTarget, planarPosition);
        float yOffset = transform.position.y - transform.position.y;
        //Calc initial velocity based on gravity, distance and angle
        float initialVelocity = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance, 2)) / (distance * Mathf.Tan(angle) + yOffset));
        Vector3 velocity = new Vector3(0, initialVelocity * Mathf.Sin(angle), initialVelocity * Mathf.Cos(angle));
        //Calc direction to jump
        float direction = Vector3.Angle(Vector3.forward, planarTarget - planarPosition) * (playerPos.x > transform.position.x ? 1 : -1);
        Vector3 finalVelocity = Quaternion.AngleAxis(direction, Vector3.up) * velocity;
        //Jump!
        Debug.Log(finalVelocity);
        rb.AddForce(finalVelocity, ForceMode2D.Impulse);
    }
    void MoveRandom()
    {
        float time;
        int rand;
        do
        {
            rand = Random.Range(0, waypoints.Count);
        } while (rand == prevWaypoint);
        if (Mathf.Abs(prevWaypoint - rand) == 2)
        {
            time = 4f;
        }
        else
        {
            time = 2f;
        }
        prevWaypoint = rand;
        StartCoroutine(Move(waypoints[rand].position, time));
    }

    IEnumerator Move(Vector3 waypoint, float time)
    {
        walkSource.loop = true;
        walkSource.clip = a_Walk;
        if(walkSource.isPlaying == false)
        {
            Debug.Log("Playing");
            walkSource.Play();
        }
        animatorState.Play("Walk");
        if (transform.position.x - waypoint.x < 0)
        {
            rotation.Rotate(1);
            eyes.transform.localRotation = Quaternion.Euler(0, -90, 0);
        }
        else
        {
            rotation.Rotate(2);
            eyes.transform.localRotation = Quaternion.Euler(0, 90, 0);
        }
        float timeElapsed = 0;
        Vector3 startPos = transform.position;
        while (timeElapsed < time && !attacking)
        {
            if(currentStatus == enemyStatus.Death)
            {
                yield break;
            }
            transform.position = Vector3.Lerp(startPos, waypoint, (timeElapsed / time));
            timeElapsed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        moving = false;
        StartCoroutine(AudioFadeOut(walkSource, 0.2f));
        if (!attacking)
        {
            rotation.Rotate(3);
            animatorState.Play("Idle");
        }
    }

    void OnCollisionEnter2D(Collision2D collider)
    {
        if (attacking)
        { 
        if(collider.collider.name == "Player")
        {
            currentStatus = enemyStatus.Victory;
            animatorState.Play("Victory");
            player.GetComponent<PlayerMovement>().Death();
        }
        else
        {
            if (currentStatus != enemyStatus.Death && currentStatus != enemyStatus.Victory)
            {
                animatorState.Play("Vulnerable");
                SetStatus((int)enemyStatus.Vulnerable);
                attacking = false;
                if(attackSource.isPlaying == true)
                    {
                        StartCoroutine(AudioFadeOut(attackSource, 0.1f));
                    }
            }
        }
        }
    }
}
