﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallTriggerTest : TriggerTarget {
    private bool toggle = false;
    protected override void TriggerOn()
    {
		base.TriggerOn ();
        if (!toggle)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + 4, transform.position.z);
        }
        toggle = !toggle;
    }

    protected override void TriggerOff()
    {
		base.TriggerOff ();
        if(toggle)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - 4, transform.position.z);
        }
        toggle = !toggle;
    }
}
