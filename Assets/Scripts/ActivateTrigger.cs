﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateTrigger : MonoBehaviour {
    public TriggerTarget target;
    private bool isTriggering = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        { 
            if(!isTriggering)
            {
                target.Triggered();
                Debug.Log("Triggered");
            }
            isTriggering = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (isTriggering)
            {
                isTriggering = false;
            }
        }
    }
}
