﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeyserTrigger : TriggerTarget {

	public ParticleSystem m_Water;
    public AudioClip geyserLoop;

	public override void Start ()
	{
		if (m_Water == null) {
			m_Water = GetComponentInChildren<ParticleSystem> ();
		}
		base.Start ();
		if (triggerStatus) {
			TriggerOn ();
		} else {
			TriggerOff ();
		}
	}

    void Update()
    {
        if(SoundPlayer.isPlaying == false)
        {
            SoundPlayer.loop = true;
            SoundPlayer.clip = geyserLoop;
            SoundPlayer.Play();
        }
    }

    protected override void TriggerOn()
    {
		base.TriggerOn ();
        GetComponent<AreaEffector2D>().enabled = true;
		if (m_Water != null) {
			m_Water.Play ();
		}
    }

    protected override void TriggerOff()
    {
		base.TriggerOff ();
        GetComponent<AreaEffector2D>().enabled = false;
		if (m_Water != null) {
			m_Water.Stop ();
		}

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Interactable")
        {
            collision.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Interactable")
        {
            collision.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        }
    }
}
