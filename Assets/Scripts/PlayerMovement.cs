﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	public StoryPassageCache story;

    Rigidbody2D rb;
    public float maxSpeed = 5f;
    public float jumpSpeed = 1000f;
    public float throwPower;
    public float direction;
    private float ledgeTimer;
    public bool isGrounded;
    private bool climbing = false;
    private bool climbBool = false;
    public bool hoverCheck = false;
    public bool isHovering = false;
    public bool isFacingRight = true;
    public GameObject highlightObject;
    public GameObject enemyObject;
    public GameObject selectedObject;
    public GameObject groundObject;
    public GameObject restartButton;
    public GameObject menuButton;
    public GameObject deathText;
    public Transform ground;
    public Transform climbCheck;
    [SerializeField] private LayerMask groundLayerMask;
    private LineRenderer lineRenderer;

    //Camera
    public CameraFollow cameraSwap;

    private Vector2 pointTop;
    private Vector2 pointMiddle;
    private Vector2 pointBottom;
    // Hover testing
    private float hoverTimer;
    //Move object / throw testing
    private Vector2 objectPos = new Vector2();
    private float bezierFloat = 5.0f;
    // Use this for initialization
	public Vector3 previousPosition;
    public Animator animatorState;
    // Audio
    public AudioSource audioSource;
    public AudioSource jumpSource;
    public AudioClip runClip;
    public AudioClip jumpClip;
    public AudioClip deathClip;

    void Start()
    {
        audioSource.clip = runClip;
        lineRenderer = GetComponent<LineRenderer>();
        hoverTimer = 0;
        rb = GetComponent<Rigidbody2D>();
        lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
    }

    // Update is called once per frame
    void Update()
    {
        if(!climbing)
            { 
            if (isFacingRight)
            {
                pointTop = new Vector2(transform.position.x, transform.position.y + 3);
                pointMiddle = new Vector2(transform.position.x + 4, transform.position.y);
                pointBottom = new Vector2(transform.position.x, transform.position.y - 3);
            }
            if (!isFacingRight)
            {
                pointTop = new Vector2(transform.position.x, transform.position.y + 3);
                pointMiddle = new Vector2(transform.position.x - 4, transform.position.y);
                pointBottom = new Vector2(transform.position.x, transform.position.y - 3);
            }

            if (Input.GetKeyDown(KeyCode.Space) && isGrounded && !climbing)
            {
                jumpSource.PlayOneShot(jumpClip);
                Jump();
            }
            if (Input.GetKey(KeyCode.Space) && !isGrounded && rb.velocity.y <= 0 && !hoverCheck && !climbing)
            {
                Hover();
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                rb.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
                hoverCheck = true;
                isHovering = false;
                animatorState.SetBool("Hovering", isHovering);
            }
            if (Input.GetKeyDown(KeyCode.G))
            {
                GetObject();
            }
            if (Input.GetKeyDown(KeyCode.F))
            {
                ThrowObject();
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
                MindControl();
            }
            if(Input.GetKeyUp(KeyCode.UpArrow))
                {
                    rb.gravityScale = 2.5f;
                }
            if (transform.position.y <= -20)
            {
                Death();
            }
            direction = Input.GetAxis("Horizontal");
        }
    }
    void FixedUpdate()
    {
        if (!climbing)
        {
            if (ledgeTimer > 0)
            {
                ledgeTimer -= Time.deltaTime;
            }
            if (ledgeTimer <= 0)
            {
                ledgeTimer = 0f;
            }
            isGrounded = false;
            HandleMovement(direction);
        }
        else
        {
            rb.gravityScale = 0;
            rb.velocity = Vector2.zero;
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                climbing = false;
                rb.gravityScale = 2.5f;
                rb.velocity = new Vector2(0, 2);
                ledgeTimer = 1f;
            }
            if(Input.GetKeyDown(KeyCode.UpArrow))
            {
                if(climbBool == false)
                {
                    climbBool = true;
                    StartCoroutine(ClimbLedge());
                }
            }
        }
    }

    void LateUpdate()
    {
        previousPosition = transform.position;
    }

    void HandleMovement(float direction)
    {
        RunAudio(direction);
        Collider2D[] climbCollider = Physics2D.OverlapCircleAll(climbCheck.position, 0.01f, groundLayerMask);
        Collider2D[] colliders = Physics2D.OverlapCircleAll(ground.position, 0.15f, groundLayerMask);
        for(int i = 0; i< climbCollider.Length; i++)
        {
            if (ledgeTimer == 0f)
            {
                if (climbCollider[i].gameObject != gameObject)
                {
                    if (climbCollider[i].name == "Ledge")
                    {
                        animatorState.Play("LedgeGrab");
                        climbing = true;
                        if(selectedObject != null)
                        {
                            GetObject();
                        }
                        rb.velocity = Vector2.zero;
                        return;
                    }
                    else
                    {
                        climbing = false;
                    }
                }
            }
        }
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                if(colliders[i].isTrigger == false)
                {
                    groundObject = colliders[i].gameObject;
                    isGrounded = true;
                    hoverCheck = false;
                    hoverTimer = 0;
                }
            }
            else
            {
                groundObject = null;
            }
        }
        if (selectedObject != null)
        {
            selectedObject.GetComponent<Rigidbody2D>().isKinematic = true;
            //selectedObject.GetComponent<Rigidbody2D>().velocity = new Vector2(direction * maxSpeed, rb.velocity.y);
            if (Input.GetKey(KeyCode.UpArrow))
            {
                bezierFloat -= 0.1f;
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                bezierFloat += 0.1f;
            }
            FollowBezierCurve();
            selectedObject.transform.position = objectPos;
        }
        rb.velocity = new Vector2(direction * maxSpeed, rb.velocity.y);

        if (Mathf.Abs(rb.velocity.x) > maxSpeed)
        {
            rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);
        }
        if (Mathf.Abs(rb.velocity.y) > 10)
        {
            rb.velocity = new Vector2(rb.velocity.x, Mathf.Sign(rb.velocity.y) * 10);
        }
        Collider2D[] objects = Physics2D.OverlapCircleAll(transform.position, 2f);
        bool interacting = false;
        bool mindControl = false;
        for (int i = 0; i < objects.Length; i++)
        {
            if (objects[i].gameObject.tag == "Interactable")
            {
                highlightObject = objects[i].gameObject;
                interacting = true;
            }
            if(objects[i].gameObject.tag == "Enemy")
            {
                enemyObject = objects[i].gameObject;
                mindControl = true;
            }
        }
        if (!interacting)
        {
            highlightObject = null;
        }
        if(!mindControl)
        {
            enemyObject = null;
        }
        if (rb.velocity.x < 0)
        {
            animatorState.SetFloat("Speed", Mathf.Abs(direction));
            isFacingRight = false;
            transform.localRotation = Quaternion.Euler(0, -180, 0);
        }
        if (rb.velocity.x > 0)
        {
            animatorState.SetFloat("Speed", Mathf.Abs(direction));
            isFacingRight = true;
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
        if(rb.velocity.y < 0 && !isHovering &&!isGrounded)
        {
            animatorState.Play("Jump_Land");
        }
        if(rb.velocity.y == 0 && !isGrounded)
        {
            if (!animatorState.GetCurrentAnimatorStateInfo(0).IsName("Jump_Air"))
            {
                animatorState.Play("Jump_Air");
            }
        }
        if (rb.velocity.x == 0)
        {
            animatorState.SetFloat("Speed", -1f);
        }
        animatorState.SetBool("Jumping", !isGrounded);
        
    }
    
    IEnumerator ClimbLedge()
    {
        animatorState.Play("LedgeClimb");
        float timeElapsed = 0;
        float time = 0.5f;
        Vector3 startingLocation = transform.position;
        Vector3 endLocation = new Vector3(transform.position.x, transform.position.y + 1.9f, transform.position.z);
        while (timeElapsed < time)
        {
            transform.position = Vector3.Lerp(startingLocation, endLocation, (timeElapsed / time));
            timeElapsed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        timeElapsed = 0;
        time = 0.5f;
        startingLocation = transform.position;
        if(isFacingRight)
        {
            endLocation = new Vector3(transform.position.x + 1f, transform.position.y, transform.position.z);
        }
        else
        {
            endLocation = new Vector3(transform.position.x - 1f, transform.position.y, transform.position.z);
        }
        while (timeElapsed < time)
        {
            transform.position = Vector3.Lerp(startingLocation, endLocation, (timeElapsed / time));
            timeElapsed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        climbing = false;
        climbBool = false;
        rb.gravityScale = 2.5f;
        animatorState.Play("Idle");

    }

    public IEnumerator Stunned()
    {
        Debug.Log("stunned");
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
        yield return new WaitForSeconds(3f);
        rb.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
    }
    public void Death()
    {
        rb.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezePositionX;
        //restartButton.SetActive(true);
        //menuButton.SetActive(true);
        //deathText.SetActive(true);
        audioSource.Stop();
        audioSource.volume = 1f;
        audioSource.PlayOneShot(deathClip);
        animatorState.Play("Death");
		DeathMenu deathMenu = FindObjectOfType<DeathMenu>();
		if (deathMenu != null) {
			deathMenu.Show (true);
		}

        this.enabled = false;
    }

    void FollowBezierCurve()
    {
        if (bezierFloat >= 9)
        {
            bezierFloat = 9;
        }
        if (bezierFloat <= 0)
        {
            bezierFloat = 0;
        }
        float point;
        Vector2 position = new Vector2();
        point = bezierFloat / (10 - 1.0f);
        position = (1.0f - point) * (1.0f - point) * pointTop + 2.0f * (1.0f - point) * point * pointMiddle + point * point * pointBottom;
        objectPos = position;
    }

    void Jump()
    {
        animatorState.CrossFade("Jump_Takeoff", 0.2f);
        rb.AddForce(new Vector2(0f, jumpSpeed));
        if (selectedObject != null)
        {            selectedObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpSpeed));
        }
    }

    void Hover()
    {
        isHovering = true;
        animatorState.SetBool("Hovering", isHovering);
        rb.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
        hoverTimer += Time.deltaTime;
        if (hoverTimer >= 1f)
        {
            isHovering = false;
            animatorState.SetBool("Hovering", isHovering);
            hoverCheck = true;
            rb.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
        }
    }

    void GetObject()
    {
        if (selectedObject == null && highlightObject != null)
        {
            selectedObject = highlightObject;
            selectedObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            bezierFloat = 5f;
        }
        else if (selectedObject != null)
        {
            selectedObject.GetComponent<Rigidbody2D>().isKinematic = false;
            selectedObject = null;
        }
    }

    void ThrowObject()
    {
        if (selectedObject != null)
        {
            selectedObject.GetComponent<Rigidbody2D>().isKinematic = false;
            selectedObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            selectedObject.GetComponent<ReleaseObject>().thrown = true;
            selectedObject.GetComponent<Rigidbody2D>().AddForce((selectedObject.transform.position - transform.position) * throwPower);
            selectedObject = null;
        }
    }

    void MindControl()
    {
        Debug.Log("Mind control!");
        if(enemyObject != null)
        {
            if (enemyObject.gameObject.GetComponent<Enemy>().currentStatus == Enemy.enemyStatus.Vulnerable)
            {
                    enemyObject.gameObject.GetComponent<Zunrtaur>().mindControlled = true;
                    this.enabled = false;
                    cameraSwap.GetComponent<CameraFollow>().player = enemyObject.gameObject;
            }
            else
            {
                cameraSwap.GetComponent<CameraFollow>().player = gameObject;
            }
        }
    }

    void RunAudio(float direction)
    {
        if (rb.velocity.y == 0 && direction != 0 && !isHovering && !hoverCheck)
        {
            audioSource.volume = 1f;
            audioSource.loop = true;
            if (audioSource.isPlaying == false)
            {
                audioSource.Play();
            }
        }
        if(rb.velocity.y > 0 || rb.velocity.y < 0 || direction == 0)
            {
                if (audioSource.volume != 0)
                {
                    StartCoroutine(AudioFadeOut());
                }
                
            }
    }

    IEnumerator AudioFadeOut()
    {
        float startVolume = audioSource.volume;

        while(audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / 0.5f;
            yield return null;
        }
        audioSource.volume = 0f;
    }
    void DrawBezierCurve()
    {/*
        lineRenderer.startColor = Color.white;
        lineRenderer.endColor = Color.white;
        lineRenderer.startWidth = 0.2f;
        lineRenderer.endWidth = 0.2f;
        lineRenderer.positionCount = 20;
        Vector2 position = new Vector2();
        float t;
        for (int i = 0; i < 20; i++)
        {
            t = i / (20 - 1.0f);
            position = (1.0f - t) * (1.0f - t) * pointTop
               + 2.0f * (1.0f - t) * t * pointMiddle
               + t * t * pointBottom;
            lineRenderer.SetPosition(i, position);
        }
*/
    }
    void OnDrawGizmos()
    {/*
		Gizmos.color = Color.red;
		Gizmos.DrawSphere (transform.position, 2f);
		*/
    }

}

