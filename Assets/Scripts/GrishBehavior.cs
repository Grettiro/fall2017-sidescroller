﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrishBehavior : Enemy {
    
    private float turnTime;
    private float randomTurnTime;
    // Use this for initialization
    protected override void Start()
    {
        StartCoroutine(PlayIdleSound());
        attackSource.clip = a_Attack;
        SetStatus((int)enemyStatus.Idle);
        fieldOfView = 45f;
        viewDistance = 15f;
        randomTurnTime = Random.Range(1f, 3f);
        base.Start();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Idle
        if (currentStatus == enemyStatus.Idle)
        {
            
            if (Detect())
            {
                SetStatus((int)enemyStatus.Attacking);
            }
            if (turnTime > randomTurnTime)
            {
                if (facingLeft)
                {
                    randomTurnTime = Random.Range(1f, 3f);
                    StartCoroutine(Lerp(Quaternion.Euler(0, 0, 0)));
                    //transform.localRotation = Quaternion.Euler(0, 0, 0);
                    facingLeft = false;
                }
                else
                {
                    randomTurnTime = Random.Range(1f, 3f);
                    facingLeft = true;
                    StartCoroutine(Lerp(Quaternion.Euler(0, -180, 0)));
                    //transform.localRotation = Quaternion.Euler(0, -180, 0);
                }

                turnTime = 0;
            }
            turnTime += Time.deltaTime;
        }
        //Attacking
        else if (currentStatus == enemyStatus.Attacking)
        {
            if (!attacking)
            {
                rb.constraints = RigidbodyConstraints2D.None;
                rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                attackSource.Play();
                Attack();
                attacking = true;
            }

        }
        //Vulnerable
        else if (currentStatus == enemyStatus.Vulnerable)
        {
            animatorState.Play("Vulnerable");
            attacking = false;
            if (vulnerableTimer >= 3f)
            {
                SetStatus((int)enemyStatus.Idle);
                animatorState.Play("Idle");
                vulnerableTimer = 0;
            }
            vulnerableTimer += Time.deltaTime;
        }
    }
    protected override void Attack()
    {
        animatorState.Play("Attack");
        StartCoroutine(AttackDelay());
        
    }

    private IEnumerator AttackDelay()
    {
        yield return new WaitForSeconds(0.5f);
        moveSource.PlayOneShot(a_Jump);
        rb.constraints = RigidbodyConstraints2D.None;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        rb.AddForce(new Vector2(directionToPlayer.x, directionToPlayer.y + 0.7f) * 550, ForceMode2D.Force);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (attacking && currentStatus != enemyStatus.Victory)
        {
            if (collision.gameObject.name == "Player" && currentStatus != enemyStatus.Vulnerable && currentStatus != enemyStatus.Death)
            {
                currentStatus = enemyStatus.Victory;
                animatorState.Play("Victory");
                StartCoroutine(Lerp(Quaternion.Euler(0, -90, 0)));
                collision.gameObject.GetComponent<PlayerMovement>().Death();
                
            }
            else if (collision.gameObject.tag == "Interactable" && currentStatus == enemyStatus.Vulnerable)
            {
                if (collision.gameObject.GetComponent<ReleaseObject>().thrown == true)
                {
                    Death();
                    currentStatus = enemyStatus.Death;
                }
            }
            else
            {
                rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
                SetStatus((int)enemyStatus.Vulnerable);
                moveSource.PlayOneShot(a_Land);
            }
        }
        if(collision.gameObject.name == "Player" && currentStatus != enemyStatus.Vulnerable && currentStatus != enemyStatus.Death)
        {
            currentStatus = enemyStatus.Victory;
            animatorState.Play("Victory");
            StartCoroutine(Lerp(Quaternion.Euler(0, -90, 0)));
            collision.gameObject.GetComponent<PlayerMovement>().Death();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player" && currentStatus == enemyStatus.Vulnerable)
        {
            Death();
            currentStatus = enemyStatus.Death;
        }
        else if(collision.gameObject.tag == "Interactable" && currentStatus == enemyStatus.Vulnerable && collision.gameObject.GetComponent<ReleaseObject>().thrown == true)
        {
            Death();
            currentStatus = enemyStatus.Death;
        }
    }


}
