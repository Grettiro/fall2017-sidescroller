﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpTextMenu : TriggerTarget {
	
	public Text HelpInfo;

	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Alpha1))
			{
			DisplayText ("Whats up ", 2f);
			}
		if (Input.GetKeyDown (KeyCode.Alpha2)) 
		{
			DisplayText (" So far So Good ", 2f);
		}
		if (Input.GetKeyDown (KeyCode.Alpha3)) 
		{
			DisplayText ("Keep Going", 2f);
		}
		if (Input.GetKeyDown (KeyCode.Alpha4)) 
		{
			DisplayText ("Yay you did it", 2f);
		}
	}

	public void DisplayText(string text , float Showtime)
	{
		HelpInfo.text = text;
		HelpInfo.gameObject.SetActive (true);
		StartCoroutine (DisplayTime (Showtime));

	}
	IEnumerator DisplayTime (float waitTime)
	{
		yield return new WaitForSeconds (waitTime);
		HelpInfo.gameObject.SetActive (false);
	}
}
