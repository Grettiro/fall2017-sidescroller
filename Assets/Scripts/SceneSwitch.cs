﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneSwitch : MonoBehaviour {

	// Type on the inspector which scene you want to load.
	public string levelToLoad;

	// Load the main menu.
	public void Return () {
		SceneManager.LoadScene (levelToLoad);
	}
}