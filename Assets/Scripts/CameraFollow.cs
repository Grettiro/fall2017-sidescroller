﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
	public GameObject player;
    
    //Changeable smoothing variables, for a floatier camera movement
    public float smoothX;
    public float smoothY;
    public float cameraShake;
    private Vector3 startingPos;

    private Vector2 vector;
    private void Start()
    {
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y, transform.position.z);
    }

    void Update()
    {
        if (cameraShake > 0)
        {
            transform.position = startingPos + Random.insideUnitSphere * 1.0f;
            cameraShake -= Time.deltaTime * 1.5f;
        }
    }
    void FixedUpdate () {
        float xPos = Mathf.SmoothDamp(transform.position.x, player.transform.position.x, ref vector.x, smoothX);
        float yPos = Mathf.SmoothDamp(transform.position.y, player.transform.position.y, ref vector.y, smoothY);

        if(yPos <1)
        {
            yPos = 1;
        }
        transform.position = new Vector3(xPos, yPos, transform.position.z);
	}

    public void StartShake()
    {
        startingPos = transform.localPosition;
    }
}
