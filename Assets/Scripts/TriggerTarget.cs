﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTarget : MonoBehaviour{
    public bool triggerStatus;
	public AudioClip SwitchOn;
	public AudioClip SwitchOff;
	protected AudioSource SoundPlayer;
	// Use this for initialization

	public virtual void Start()
	{
		SoundPlayer = GetComponent<AudioSource> ();
		if (SoundPlayer == null) 
		{
			//Debug.LogError ("No Audio " + gameObject.name); 
		}
	}

	// Update is called once per frame
	public void Triggered()
    {
        if(triggerStatus)
        {
            TriggerOff();
        }
        else
        {
            TriggerOn();
        }
        triggerStatus = !triggerStatus;
    }
    
    virtual protected void TriggerOn()
    {
        Debug.Log("Shouldn't be called");
		if (SoundPlayer != null && SwitchOn != null) 
		{
            SoundPlayer.loop = false;
			SoundPlayer.clip = SwitchOn;
			SoundPlayer.PlayOneShot(SwitchOn);

		} else 
		{
			Debug.Log ("Does not have Audio " + gameObject.name +"TriggerOn()");	
		}
    }
    
    virtual protected void TriggerOff()
    {
        Debug.Log("Shouldn't be called");
		if (SoundPlayer != null && SwitchOff != null) 
		{	
			SoundPlayer.clip = SwitchOff;
			SoundPlayer.PlayOneShot (SwitchOff, 1f);
		} 
		else 
		{
			Debug.Log ("Audio Not Req " + gameObject.name +"TriggerOff()");	
		}

    }

    public bool isTriggerOn()
    {
        return triggerStatus;
    }
}
