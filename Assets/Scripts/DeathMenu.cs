﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathMenu : MonoBehaviour 
{
	public GameObject m_Background;

	public void Show(bool show)
	{
		m_Background.SetActive (show);
	}
}
