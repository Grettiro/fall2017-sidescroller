﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxScoller : MonoBehaviour 
{
	#region Variables
	[SerializeField] float m_Speed = 1;
	PlayerMovement m_PlayerMover;
	StoryDialogMenu m_StoryMenu;
	#endregion

	#region Properties
	float PlayerCurrentPosX { get { return m_PlayerMover.transform.position.x; } }
	float PlayerLastPosX { get { return m_PlayerMover.previousPosition.x; } }
	float PlayerMovement { get { return PlayerCurrentPosX - PlayerLastPosX; } }
	#endregion

	#region Functions
	// Use this for initialization
	void Awake () 
	{
		m_PlayerMover = FindObjectOfType<PlayerMovement> ();
		m_StoryMenu = FindObjectOfType<StoryDialogMenu> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Time.timeScale == 0 || (m_StoryMenu != null && m_StoryMenu.background.gameObject.activeSelf)) {
			return;
		}
		Vector3 backgroundPos = transform.position;
		backgroundPos.x -= PlayerMovement * m_Speed;
		transform.position = backgroundPos;
	}
	#endregion
}
