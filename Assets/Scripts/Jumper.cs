﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumper : Enemy
{
    private float jumpCooldown = 4.5f;
    public float initialAngle = 45;
    private Vector3 initialPosition;
    private List<Transform> waypoints = new List<Transform>();
    private bool jumping = false;
    private bool turned = false;
    private float attackCooldown;

    public AudioClip a_Attack2;
    public AudioClip a_Attack3;
    public AudioClip[] randomAttackClip;
    //Setting up an integer that negates jumper from jumping to the same location twice in a row
    private int prevWaypoint = 2;
    protected override void Start()
    {
        StartCoroutine(PlayIdleSound());
        randomAttackClip = new AudioClip[3];
        randomAttackClip[0] = a_Attack;
        randomAttackClip[1] = a_Attack2;
        randomAttackClip[2] = a_Attack3;
        base.Start();
        fieldOfView = 90f;
        viewDistance = 10f;
        initialPosition = transform.position;
        Transform[] tempWaypoints = transform.parent.GetComponentsInChildren<Transform>();
        foreach(Transform tWaypoint in tempWaypoints)
        {
            if(tWaypoint.name.Contains("Waypoint"))
            {
                waypoints.Add(tWaypoint);
            } 
        }
    }

    private void FixedUpdate()
    {
        if(currentStatus == enemyStatus.Idle)
        {
            jumpCooldown += Time.deltaTime;
            
            if (Detect() && !attacking && !jumping)
            {
                SetStatus((int)enemyStatus.Attacking);
            }
            else
            {
                if(jumpCooldown >= 0.5f && jumpCooldown <= 1f && !jumping)
                {
                    animatorState.Play("Idle");
                }
                if (jumpCooldown >= 4.5f && !turned)
                {
                    turned = true;
                    animatorState.Play("Jump");
                }
                if (jumpCooldown >= 5f)
                {
                        jumping = true;
                        JumpRandom();
                        turned = false;
                        jumpCooldown = 0;
                }
            }
        }
        if(currentStatus == enemyStatus.Attacking)
        {
            if (!attacking)
            {
                if (attackCooldown >= 1f)
                {
                    attacking = true;
                    Attack();
                    attackCooldown = 0f;
                }
                attackCooldown += Time.deltaTime;
            }
            
        }
        if (currentStatus == enemyStatus.Vulnerable)
        {
            if (vulnerableTimer >= 1.5f)
            {
                vulnerableTimer = 0;
                animatorState.Play("Idle");
                currentStatus = enemyStatus.Idle;
            }
            vulnerableTimer += Time.deltaTime;
        }
        if(jumping == true)
        {
            if(rb.velocity.y < 0)
            {
                animatorState.Play("JumpAir");
            }
        }
    }

    protected override void Attack()
    {
        attackSource.PlayOneShot(randomAttackClip[Random.Range(0, 3)]);
        animatorState.Play("Attack");
        StartCoroutine(AttackAnimation());
    }

    private void JumpRandom()
    {
        int rand;
        do
        {
            rand = Random.Range(0, waypoints.Count);
        } while (rand == prevWaypoint);
        prevWaypoint = rand;
        moveSource.PlayOneShot(a_Jump);
        Jump(waypoints[rand].position);
    }
    private void Jump(Vector3 waypoint)
    {
        //Initialize float variables
        float gravity = Physics.gravity.magnitude * 2.5f;
        float angle = initialAngle * Mathf.Deg2Rad;
        //Initialize target position and origin position
        Vector3 planarTarget = new Vector3(waypoint.x, 0, waypoint.z);
        Vector3 planarPosition = new Vector3(transform.position.x, 0, transform.position.z);
        //Calculate distance and difference between y pos
        float distance = Vector3.Distance(planarTarget, planarPosition);
        float yOffset = transform.position.y - waypoint.y;
        //Calc initial velocity based on gravity, distance and angle
        float initialVelocity = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance, 2)) / (distance * Mathf.Tan(angle) + yOffset));
        Vector3 velocity = new Vector3(0, initialVelocity * Mathf.Sin(angle), initialVelocity * Mathf.Cos(angle));
        //Calc direction to jump
        float direction = Vector3.Angle(Vector3.forward, planarTarget - planarPosition) * (waypoint.x > transform.position.x ? 1 : -1);
        Vector3 finalVelocity = Quaternion.AngleAxis(direction, Vector3.up) * velocity;
        //Jump!
            rb.AddForce(finalVelocity, ForceMode2D.Impulse);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            player.GetComponent<PlayerMovement>().Death();
            currentStatus = enemyStatus.Victory;
            animatorState.Play("Victory");
        }
        else if (currentStatus != enemyStatus.Death && currentStatus != enemyStatus.Victory)
        {
            if (jumping)
            {
                animatorState.Play("JumpLand");
                jumping = false;
            }
            if(attacking)
            {
                animatorState.Play("Idle");
                StartCoroutine(AttackDelay());
               currentStatus = enemyStatus.Vulnerable;
            }
        }
        moveSource.PlayOneShot(a_Land);
    }
    IEnumerator AttackDelay()
    {
        yield return new WaitForSeconds(0.5f);
        attacking = false;
    }
    IEnumerator AttackAnimation()
    {
        yield return new WaitForSeconds(0.5f);
        Jump(player.transform.position);
    }
    protected override IEnumerator Lerp(Quaternion point)
    {
        float timeElapsed = 0;
        float time = 0.25f;
        Quaternion startingRotation = transform.parent.localRotation;
        while (timeElapsed < time)
        {
            transform.parent.localRotation = Quaternion.Lerp(startingRotation, point, (timeElapsed / time));
            timeElapsed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
}
