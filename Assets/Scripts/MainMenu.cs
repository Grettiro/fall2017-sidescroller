﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	// Author: Fabio Ribak
	// This script make the buttons from the main menu work.

	public Canvas MainCanvas;

	// The background images used for the menu system.
	public GameObject menuScreen;
	public GameObject controlsScreen;
	public GameObject creditsScreen;
	public GameObject quitScreen;

	// Type on the inspector the name of the first level.
	public string levelToLoad;

	// When the Play button is pressed, load the first level.
	public void PlayGame () {
		SceneManager.LoadScene (levelToLoad);
	}

	// When the Controls button is pressed, disable the Main Menu and enable the Controls page.
	public void Controls () {
		menuScreen.SetActive (false);
		controlsScreen.SetActive (true);
	}

	public void Return () {
		// When the Return button is played, disable the Controls Screen and enable the Main Menu again.
		if (controlsScreen) {
			controlsScreen.SetActive (false);
			menuScreen.SetActive (true);
		}

		if (creditsScreen) {
			creditsScreen.SetActive (false);
			menuScreen.SetActive (true);
		}

		// When the No button is pressed, disable the Quit Screen and enable the Main Menu again.
		if (quitScreen) {
			quitScreen.SetActive (false);
			menuScreen.SetActive (true);
		}
	}

	// When the Credits button is pressed, disable the Main Menu and enagle the Credits Screen.
	public void Credits () {
		menuScreen.SetActive (false);
		creditsScreen.SetActive (true);
	}

	// When the Quit button is pressed, disable the Main Menu and enable the Quit Screen.
	public void QuitScene () {
		menuScreen.SetActive (false);
		quitScreen.SetActive (true);
	}

	// When the Yes button is pressed, close the game.
	public void QuitGame () {
		Application.Quit ();
	}
}
