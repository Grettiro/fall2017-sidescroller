﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour {

	// Author: Fabio Ribak
	// This script rolls the credits text up, and loads the main menu again at the end.

	public GameObject rollingText;
	public GameObject menuScreen;
	public GameObject creditsScreen;
	public int textSpeed;
	public float creditsDuration;

	private float timeToLoad; // How long until the Main Menu is loaded.
	private bool isTimeRunning = true;

	void Update () {
		// Roll the credits text up


		// Load the Main Menu and disable the Credits Screen when time reaches X seconds.
		if (timeToLoad > creditsDuration)
			LoadMenu ();

		// Start running time to load the Main Menu scene.
		if (isTimeRunning)
			timeToLoad += Time.deltaTime;

		// If you press Esc, go back to the Main Menu.
		if (Input.GetKey("escape")) LoadMenu ();
		}

	// Load the Main Menu again and disable the Credits Screen.
	void LoadMenu() {
		creditsScreen.SetActive (false);
		menuScreen.SetActive (true);
		}
}
