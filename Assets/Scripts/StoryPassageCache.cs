﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryPassageCache{

    private Object[] storyText;
    public StoryPassageCache()
    {
        storyText = Resources.LoadAll("Story", typeof(TextAsset));
    }
	public TextAsset StoryTextAsset(int index)
	{
		return (TextAsset)storyText [index];
	}

    public List<TextAsset> StoryCacheReturn(int from, int to)
    {
        List<TextAsset> tempText = new List<TextAsset>();
        for (int i = 0; i < storyText.Length; i++)
        {
            if(i >= from && i <= to)
            {
                tempText.Add((TextAsset)storyText[i]);
            }
        }
        return tempText;
    }
}
