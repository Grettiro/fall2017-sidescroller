﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikesTrigger : TriggerTarget {

    protected override void TriggerOn()
    {
        GetComponent<Rigidbody2D>().isKinematic = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.name == "Player")
        {
            collision.gameObject.GetComponent<PlayerMovement>().Death();
        }
        Destroy(gameObject);
    }
}
