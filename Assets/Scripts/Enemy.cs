﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    protected float fieldOfView;
    protected float viewDistance;

    protected GameObject player;
    public Animator animatorState;
    public Transform eyes;
    protected Vector3 directionToPlayer;
    protected Rigidbody2D rb;
    public enum enemyStatus { Idle, Vulnerable, Attacking, Hit, Death, Victory};
    public enemyStatus currentStatus;
    public bool mindControlled = false;
    [SerializeField]
    protected bool facingLeft = true;
    protected bool attacking = false;
    protected float vulnerableTimer;
    //Disable collision box
    [SerializeField]
    protected Collider2D collisionBox;
    //Mind Control initialization
    public Transform groundCheck;
    public ZunrtaurRotationHelper rotation;
    [SerializeField] protected LayerMask groundLayerMask;
    //Audio
    [Header("AudioAttributes")]
    public AudioClip a_Attack;
    public AudioClip a_Walk;
    public AudioClip a_Jump;
    public AudioClip a_Land;
    public AudioClip a_Idle1;
    public AudioClip a_Idle2;
    public AudioClip a_Death;
    public AudioSource attackSource;
    public AudioSource moveSource;
    protected float eyeLocationZ;
    protected AudioClip[] randomIdle;

    protected virtual void Start()
    {

        eyeLocationZ = transform.position.z;
        randomIdle = new AudioClip[2];
        randomIdle[0] = a_Idle1;
        randomIdle[1] = a_Idle2;
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody2D>();
		collisionBox = GetComponent<Collider2D> ();
    }
    virtual protected bool Detect()
    {
        
        directionToPlayer = (player.transform.position - eyes.position).normalized;
        if (Vector3.Dot(directionToPlayer, eyes.forward) < 0)
        {
            return false;
        }

        if (Vector3.Angle(eyes.forward, directionToPlayer) < fieldOfView)
        {
            float distanceToPlayer = Vector3.Distance(eyes.position, player.transform.position);
            if (distanceToPlayer < viewDistance)
            {
                RaycastHit2D hit = Physics2D.Raycast(eyes.position, directionToPlayer, distanceToPlayer);
                if (hit.collider != null && hit.collider.name == player.name)
                {
                    return true;
                }
            }

        }
        return false;
    }

    virtual protected void Attack()
    {
        Debug.Log("BaseAttackClass");
    }

    virtual protected void Death()
    {
        animatorState.Play("Death");
        currentStatus = enemyStatus.Death;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        GetComponent<Rigidbody2D>().isKinematic = true;
        collisionBox.enabled = false;
        Destroy(gameObject, 2f);
        attackSource.Stop();
        moveSource.PlayOneShot(a_Death);
    }

    protected void SetStatus(int status)
    {
        currentStatus = (enemyStatus)status;
    }

    virtual protected void HandleMovement(float direction)
    {
        float maxSpeed = 6f;
        bool isGrounded = true;
        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, 0.10f, groundLayerMask);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                isGrounded = true;
            }
        }
        rb.velocity = new Vector2(direction * maxSpeed, rb.velocity.y);

        if (Mathf.Abs(rb.velocity.x) > maxSpeed)
        {
            rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);
        }
        if (Mathf.Abs(rb.velocity.y) > 10)
        {
            rb.velocity = new Vector2(rb.velocity.x, Mathf.Sign(rb.velocity.y) * 10);
        }
        if (rb.velocity.x < 0)
        {
            facingLeft = true;
            rotation.Rotate(2);
        }
        if (rb.velocity.x > 0)
        {
            facingLeft = false;
            rotation.Rotate(1);
        }
        if (rb.velocity.x == 0)
        {
            rotation.Rotate(3);
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            currentStatus = enemyStatus.Idle;
            animatorState.Play("Idle");
            vulnerableTimer = 0;
            player.GetComponent<PlayerMovement>().enabled = true;
            mindControlled = false;
            rotation.Rotate(3);
        }
    }
    virtual protected IEnumerator Lerp(Quaternion point)
    {
        float timeElapsed = 0;
        float time = 0.25f;
        Quaternion startingRotation = transform.localRotation;
        while (timeElapsed < time)
        {
            transform.localRotation = Quaternion.Lerp(startingRotation, point, (timeElapsed / time));
            timeElapsed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    virtual protected IEnumerator PlayIdleSound()
    {
        yield return new WaitForSeconds(Random.Range(3f, 6f));
        int index = Random.Range(0, 2);
        if (moveSource.isPlaying == false)
        {
            if (currentStatus == enemyStatus.Idle)
            {
                moveSource.volume = 1f;
                moveSource.PlayOneShot(randomIdle[index]);
                yield return new WaitForSeconds(randomIdle[index].length);
            }

        }
        StartCoroutine(PlayIdleSound());
    }

    public void CallDeath()
    {
        Death();
    }

    virtual protected IEnumerator AudioFadeOut(AudioSource audioSource, float speed)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / speed;
            yield return null;
        }
        audioSource.volume = 0f;
        audioSource.Stop();
        audioSource.volume = 1f;
    }
}
