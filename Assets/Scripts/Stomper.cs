﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stomper : Enemy {
    private float attackDelay = 0;
    private bool attacked = false;
    private bool turning = false;
    [SerializeField]
    private Transform ground;
    [SerializeField]
    private Transform rotationTransform;
    public CameraFollow cameraFollow;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        Debug.Log(eyeLocationZ);
        fieldOfView = 90f;
        viewDistance = 15f;
        moveSource.clip = a_Walk;
        moveSource.loop = true;
        attackSource.clip = a_Attack;
        animatorState.Play("Walk");
    }

    // Update is called once per frame
    void FixedUpdate () {
		if(currentStatus == enemyStatus.Idle)
        {
            if(Detect() && !attacking)
            {
                SetStatus((int)enemyStatus.Attacking);
            }
            else
            {
                if (moveSource.isPlaying == false)
                {
                    moveSource.volume = 1f;
                    moveSource.Play();
                }
                if (facingLeft)
                {
                    rb.velocity = new Vector2(-2f, rb.velocity.y);
                }
                else
                {
                    rb.velocity = new Vector2(2f, rb.velocity.y);
                }
            }
        }
        if(currentStatus == enemyStatus.Attacking)
        {
            Attack();
        }
        if(currentStatus == enemyStatus.Vulnerable)
        {
            vulnerableTimer += Time.deltaTime;
            if(vulnerableTimer >= 5f)
            {
                if(facingLeft)
                {
                    transform.position = new Vector3(transform.position.x + 2.2f, transform.position.y, transform.position.z);
                    attacking = true;
                    StartCoroutine(Lerp(Quaternion.Euler(0, -90, 0)));
                    eyes.localRotation = Quaternion.Euler(0, -90, 0);
                    //eyes.position = new Vector3(eyes.position.x - 1, eyes.position.y, eyes.position.z);
                }
                else
                {
                    transform.position = new Vector3(transform.position.x - 2.5f, transform.position.y, transform.position.z);
                    attacking = true;
                    StartCoroutine(Lerp(Quaternion.Euler(0, 90, 0)));
                    eyes.localRotation = Quaternion.Euler(0, 90, 0);
                    //eyes.position = new Vector3(eyes.position.x + 1, eyes.position.y, eyes.position.z);
                }
                facingLeft = !facingLeft;
                Physics2D.IgnoreCollision(player.GetComponent<CapsuleCollider2D>(), GetComponent<BoxCollider2D>(), false);
                SetStatus((int)enemyStatus.Idle);
                animatorState.Play("Walk");
                vulnerableTimer = 0;
                StartCoroutine(attackReset());
            }
        }
	}

    protected override void Attack()
    { 
        rb.velocity = Vector2.zero;
        attacking = true;
        if (attackDelay >= 0.83f && attackSource.isPlaying == false)
        {
            if (moveSource.isPlaying == true)
            {
                StartCoroutine(AudioFadeOut(moveSource,0.5f));
            }
            animatorState.Play("Attack");
            attackSource.PlayOneShot(a_Attack, 1.0f);
        }
        if (attackDelay >= 2.33f && !attacked)
        {
            if (Vector3.Distance(transform.position, player.transform.position) <= 30)
            {
                cameraFollow.cameraShake = 1f;
                cameraFollow.StartShake();
            }
            Collider2D[] colliders = Physics2D.OverlapCircleAll(ground.position, 0.10f, groundLayerMask);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                {
                    if (player.GetComponent<PlayerMovement>().groundObject != null)
                    {
                        if (colliders[i].gameObject.tag == player.GetComponent<PlayerMovement>().groundObject.tag)
                        {
                            StartCoroutine(player.GetComponent<PlayerMovement>().Stunned());
                        }
                    }
                }
            }
            attacked = true;
        }
        if (attackDelay >= 3.77f)
        {
            attackDelay = 0;
            SetStatus((int)enemyStatus.Idle);
            animatorState.Play("Walk");
            attacked = false;
        }
        attackDelay += Time.deltaTime;
    }

    private IEnumerator attackReset()
    {
        Debug.Log("Derp");
        yield return new WaitForSeconds(1f);
        attacking = false;
    }

    protected override IEnumerator Lerp(Quaternion point)
    {
        float timeElapsed = 0;
        float time = 0.25f;
        Quaternion startingRotation = rotationTransform.localRotation;
        while (timeElapsed < time)
        {
            rotationTransform.localRotation = Quaternion.Lerp(startingRotation, point, (timeElapsed / time));
            timeElapsed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        attacking = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject != player && collision.gameObject.tag != "Floor")
        {
            Physics2D.IgnoreCollision(player.GetComponent<CapsuleCollider2D>(), GetComponent<BoxCollider2D>());
            SetStatus((int)enemyStatus.Vulnerable);
            animatorState.Play("Vulnerable");
            if (moveSource.isPlaying == true)
            {
                StartCoroutine(AudioFadeOut(moveSource,0.25f));
            }
        }
        if(collision.gameObject == player && currentStatus != enemyStatus.Vulnerable)
        {
            player.GetComponent<PlayerMovement>().Death();
            SetStatus((int)enemyStatus.Victory);
        }
    }
}
