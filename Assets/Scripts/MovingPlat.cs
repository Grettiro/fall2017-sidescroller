﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlat : TriggerTarget {

	float TotalTime;
	Vector3 StartPos;
	public Vector3 EndPos;
	public float JourneyTime =1;
	float TimePassed;
	bool isTowardsEnd = true;
	public bool isEnabledatStart;

	override public void Start () 
	{
		base.Start ();
		StartPos = transform.position;		
		enabled = isEnabledatStart;
	}

	 void Update ()
	{
		
		if (isTowardsEnd) 
		{
			TimePassed += Time.deltaTime;
			if (TimePassed >= JourneyTime) 
			{	
				isTowardsEnd = false;
			}
		} 
		else 
		{
			TimePassed -= Time.deltaTime;
			if(TimePassed <= 0) 
			{
				
				isTowardsEnd = true;
			}
	
		}
		TotalTime = TimePassed / JourneyTime;
		transform.position = Vector3.Lerp(StartPos, EndPos, TotalTime);		
	}
	protected override void TriggerOn()
	{
		base.TriggerOn ();
		enabled = true;
	}
	protected override void TriggerOff()
	{
		base.TriggerOff ();
		enabled = false;
	}
}
