﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gems : MonoBehaviour {
	public GameObject Gem;
	public GameObject Door;
	public bool hasGem;
	public string SceneNumber;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D col) 
	{
		if (col.gameObject.name == "Gem") 
		{
			hasGem = true;
			Destroy (col.gameObject);
		}

		if (col.gameObject.name == "FinalDoor" && hasGem == true) 
		{
			SceneManager.LoadScene (SceneNumber);
		} 
		else 
		{
			//Debug.Log (" You do not have the Gem");
		}
	}
}
