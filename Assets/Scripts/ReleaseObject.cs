﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReleaseObject : MonoBehaviour {
    public PlayerMovement player;
    public bool thrown = false;
    private float throwTimer = 0;
	Rigidbody2D rigidBody;
	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerMovement> ();
		rigidBody = GetComponent<Rigidbody2D> ();

		if (rigidBody == null) {
			enabled = false;
			Debug.LogErrorFormat ("GameObject {0} has a ReleaseObject script, but no rigid body", name);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(thrown)
        {
            if(throwTimer >= 0.2f)
            {
				if (rigidBody.velocity == Vector2.zero)
                {
                    Debug.Log("stopped");
                    thrown = false;
                    throwTimer = 0;
                    return;
                }
            }
            throwTimer += Time.deltaTime;
        }
        if(!thrown)
        {
			rigidBody.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        }
	}

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag != "Geyser" && collider.name != "Ledge")
        {
            if (player.selectedObject == gameObject)
            {
                player.selectedObject = null;
				rigidBody.isKinematic = false;
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name == "Stomper")
        {
            if (player.selectedObject == gameObject)
            {
                player.selectedObject = null;
                rigidBody.isKinematic = false;
            }
        }
    }
}
