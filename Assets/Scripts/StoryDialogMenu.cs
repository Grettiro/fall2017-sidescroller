﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryDialogMenu : MonoBehaviour {

	public Image background;
	public Text storyText;
    public StoryHandler storyHandler;
    public bool skipText;
    public float textSpeed = 0.025f;
    private int textLength = 0;

    // Use this for initialization
    void Start ()
	{
        GetComponentInParent<Canvas>().enabled = true;
		HideDialog ();
	}

	public void DisplayDialog(string text)
	{
        storyText.text = "";
        StartCoroutine(WriteText(text));
		background.gameObject.SetActive (true);
		storyText.gameObject.SetActive (true);
	}

	public void HideDialog()
	{
		background.gameObject.SetActive (false);
		storyText.gameObject.SetActive (false);
	}

    IEnumerator WriteText(string text)
    {

        foreach (char letter in text.ToCharArray())
        {
            storyText.text += letter;
            if (textSpeed > 0)
            {
                yield return new WaitForSeconds(textSpeed);
            }
        }
        if (storyHandler != null)
        {
            storyHandler.allowSkip = true;
        }
    }
}
