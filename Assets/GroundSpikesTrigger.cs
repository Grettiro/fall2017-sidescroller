﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSpikesTrigger : TriggerTarget
{

    protected override void TriggerOn()
    {
        GetComponent<BoxCollider2D>().enabled = true;
        transform.GetChild(0).gameObject.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Enemy")
        {
            collider.GetComponent<Enemy>().CallDeath();
        }
        Destroy(gameObject);
    }
}